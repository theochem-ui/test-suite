A collection of test scripts to benchmark and validate the performance of various parts of the polarizable embedding QMMM scheme operated by coupling the ASE, GPAW, and SCME software implementations.

### Requirements

* ASE
* GPAW
* SCME (C++)
* pandas

### Installation and Usage

1) `git clone git@gitlab.com:theochem-ui/test-suite.git test-suite`

2) `export PYTHONPATH=$PYTHONPATH:/path/to/test-suite`

3) Copy one of the scripts in `/test-suite/example_runfiles` to some place on your machine and execute it.