import os, shutil
import pandas as pd
from testsuite.qmmm.benchmark_Nc_qmmm import benchmark_Nc_qmmm 
import numpy as np

#Eref depends on h and k and is hardcoded in benchmark function, so it has to be changed for different h and k settings
hlist = [0.20]
klist = [1]
Nclist = [1, 2, 3, 4, 5, 6, 7, 8, 9]
Nc_outer_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
list_pd = []
for h in hlist:
    for k in klist:
        for Nc in Nclist:
            for Nc_outer in Nc_outer_list:
                if Nc_outer == 0 or Nc_outer > Nc:
                    #print("Now processing h = {:.2f}, Nc = {:d}, Nc_outer = {:d}.".format(h, Nc, Nc_outer))
                    benchmark_results = benchmark_Nc_qmmm(h,
                                                    k,
                                                    Nc,
                                                    Nc_outer,
                                                    # QMMM args
                                                    damping_value=0.82,
                                                    # QM args
                                                    xc="PBE",                                                 
                                                    # MM args
                                                    numerical=False,
                                                    debug_numerical=[1, 1, 1],
                                                    system=np.ones(1) * 1000)
                    list_pd.append(benchmark_results)
                   
df_benchmark_result= pd.concat(list_pd, ignore_index=True)
df_benchmark_result.to_csv("df_Nc_benchmark_results_h_0_2.csv")
