from testsuite.scme.dimer_mmmm import dimer_mmmm
from testsuite.gpaw.dimer_qmqm import dimer_qmqm
from testsuite.qmmm.dimer_qmmm import dimer_qmmm
import matplotlib.pyplot as plt
import numpy as np

# Run pure QM-QM interaction.
qmqm = dimer_qmqm(dmin=2.5, dmax=3.4, accurate=True)
plt.plot(qmqm["d_OO"], qmqm["E_int"], label="QM-QM")

# Run QM-MM interaction (QM is acceptor).
qmmm_acc = dimer_qmmm(dmin=2.5,
                    dmax=3.4,
                    accurate=True,
                    reverse=True,
                    damping_value=0.82,
                    system=np.ones(1) * 1000)
plt.plot(qmmm_acc["d_OO"], qmmm_acc["E_int"], label="QM_acceptor-MM")

# Run QM-MM interaction (QM is donor).
qmmm_don = dimer_qmmm(dmin=2.5,
                      dmax=3.4,
                      accurate=True,
                      reverse=False,
                      system=np.ones(1) * 1000)
plt.plot(qmmm_don["d_OO"], qmmm_don["E_int"], label="QM_donor-MM")

# Run SCME MM-MM interaction.
mmmm = dimer_mmmm(dmin=2.5,
                  dmax=3.4,
                  system=np.ones(1) * 1000)
plt.plot(mmmm["d_OO"], mmmm["E_int"], label="MM-MM")

# Plot settings.
plt.xlabel("$d_\mathrm{O-O}$ / $\mathrm{\AA}$")
plt.ylabel("$E_\mathrm{int}$ / eV")
plt.legend()
plt.savefig("dimer_binding_curves.png")
plt.show()