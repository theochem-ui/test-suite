from testsuite.qmmm.ice_Eads_qmmm import ice_Eads_qmmm
from ase.parallel import parprint
import numpy as np

for i in range(2, 3):
    Nc_outer = np.array([i, i, 0])
    parprint("\nNc_outer = ", Nc_outer)

    df = ice_Eads_qmmm(nqm=1,
                       nrand=10,
                       Nc_outer=Nc_outer,
                       accurate=True,
                       damping_value=0.82,
                       system=np.ones(1) * 1000)
    parprint(df)
    df.to_csv("ice_Eads_Nc_outer_{:d}.csv".format(i))
