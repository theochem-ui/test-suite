from testsuite.gpaw.ice_Ecoh_qmqm import ice_Ecoh_qmqm
from testsuite.scme.ice_Ecoh_mmmm import ice_Ecoh_mmmm
from testsuite.qmmm.ice_Ecoh_qmmm import ice_Ecoh_qmmm
from ase.parallel import parprint
import numpy as np

# QM-QM (Nc-independent).
qm_coh, qm_mono, qm_layer = ice_Ecoh_qmqm(accurate=True)
qm_mono.to_csv("QM_monomer_energy.csv")
qm_layer.to_csv("QM_layers_energy.csv")
parprint("E_coh_QM = ", qm_coh)

# Nc-dependent stuff.
for i in range(2, 11):
    Nc_outer = np.array([i, i, i])
    parprint("\nNc_outer = ", Nc_outer)

    # MM-MM
    mm_coh = ice_Ecoh_mmmm(Nc=Nc_outer,
                           system=np.ones(1) * 1000)
    parprint("E_coh_MM = ", mm_coh)

    # QM-MM
    df = ice_Ecoh_qmmm(nqm=48,
                       nrand=1,
                       Nc_outer=Nc_outer,
                       accurate=True,
                       damping_value=0.82,
                       system=np.ones(1) * 1000)
    parprint(df)
    df.to_csv("ice_qmmm_Nc_outer_{:d}.csv".format(i))
