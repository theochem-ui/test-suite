from testsuite.scme.dimer_mmmm import dimer_mmmm
from testsuite.gpaw.dimer_qmqm import dimer_qmqm
from testsuite.qmmm.dimer_qmmm import dimer_qmmm
import matplotlib.pyplot as plt
import numpy as np

# Run pure QM-QM interaction.
qmqm = dimer_qmqm(dmin=2.5, dmax=3.4, accurate=False)
plt.plot(qmqm["d_OO"], qmqm["E_int"], label="QM-QM")
qmqm.to_csv("qmqm.csv")

# Run QM-MM interaction (QM is acceptor).
for i in range(75, 86):
    qmmm_acc = dimer_qmmm(dmin=2.5,
                        dmax=3.4,
                        accurate=False,
                        reverse=True,
                        damping_value=i/100,
                        system=np.ones(1) * 1000)
    plt.plot(qmmm_acc["d_OO"], qmmm_acc["E_int"],
             label="QM_acceptor-MM (damping: {:.2f})".format(i/100))
    qmmm_acc.to_csv("qmmm_acc_damping_{:.2f}.csv".format(i/100))

# Run QM-MM interaction (QM is donor).
qmmm_don = dimer_qmmm(dmin=2.5,
                      dmax=3.4,
                      accurate=False,
                      reverse=False,
                      system=np.ones(1) * 1000)
plt.plot(qmmm_don["d_OO"], qmmm_don["E_int"], label="QM_donor-MM")
qmmm_don.to_csv("qmmm_don.csv")

# Run SCME MM-MM interaction.
mmmm = dimer_mmmm(dmin=2.5,
                  dmax=3.4,
                  system=np.ones(1) * 1000)
plt.plot(mmmm["d_OO"], mmmm["E_int"], label="MM-MM")
mmmm.to_csv("mmmm.csv")

# Plot settings.
plt.xlabel("$d_\mathrm{O-O}$ / $\mathrm{\AA}$")
plt.ylabel("$E_\mathrm{int}$ / eV")
plt.legend()
plt.savefig("optimize_damping_value.png")
plt.show()