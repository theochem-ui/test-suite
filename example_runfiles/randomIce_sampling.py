from testsuite.qmmm.ice_Ecoh_qmmm import ice_Ecoh_qmmm

nqm1 = ice_Ecoh_qmmm(nqm=1)
nqm48 = ice_Ecoh_qmmm(nqm=48)
nqm95 = ice_Ecoh_qmmm(nqm=95)

nqm1.to_csv("results_nqm_1.csv")
nqm48.to_csv("results_nqm_48.csv")
nqm95.to_csv("results_nqm_95.csv")
