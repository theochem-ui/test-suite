#!/bin/bash -l              
#                           
#SBATCH --job-name=ice_Ecoh_test
#SBATCH --partition=64cpu_256mem
#SBATCH --nodes=1           
#SBATCH --ntasks-per-node=64
#SBATCH --mem-per-cpu=3800  
#SBATCH --time=07-00:00:00   
#SBATCH --signal=B:USR1@180
#SBATCH --mail-type=END,FAIL

# Define backup function (called when walltime is exceeded).
backup_function()
{
    echo "  "
    echo 'Walltime exceeded! All files will be copied back to the results folder...'
    echo "  "
    mkdir -vp "${SLURM_SUBMIT_DIR}/restart"
    rsync -av "${TMP_WORK_DIR}" "${SLURM_SUBMIT_DIR}/restart/"
    echo "  "
    echo "Backup of TMP_WORK_DIR was created at $(date)"
    echo "  "
    echo "  "
    echo "### Final cleanup: Remove TMP_WORK_DIR ..."
    echo "  "
    # Remove TMP_WORK_DIR ...
    rm -rvf "${TMP_WORK_DIR}"
    # Remove temporary work directory
    if test -z "$SLURM_JOB_NUM_NODES " -o "$SLURM_JOB_NUM_NODES" = "1"; then
      rm -rvf "${TMP_WORK_DIR}"
    fi
        echo "END_TIME             = `date +'%y-%m-%d %H:%M:%S %s'`"
        echo "  "
    exit
}

# Call finalize_job function as soon as we receive USR1 signal
trap backup_function USR1

# Load scientific application.
module purge
module load ase/qmmm_paper
module load gpaw/qmmm_paper
module load scme/qmmm_paper
module load asetools
module load testsuite

# Bookkeeping.
echo " "
echo "### Printing basic job infos to stdout ..."
echo " "
echo "START_TIME             = `date +'%y-%m-%d %H:%M:%S %s'`"
echo "HOSTNAME               = ${HOSTNAME}"
echo "USER                   = ${USER}"
echo "SLURM_JOB_NAME         = ${SLURM_JOB_NAME}"
echo "SLURM_JOB_ID           = ${SLURM_JOB_ID}"
echo "SLURM_SUBMIT_DIR       = ${SLURM_SUBMIT_DIR}"
echo "SLURM_JOB_NUM_NODES    = ${SLURM_JOB_NUM_NODES}"
echo "SLURM_CPUS_ON_NODE     = ${SLURM_CPUS_ON_NODE}"
echo "SLURM_NTASKS           = ${SLURM_NTASKS}"
echo "SLURM_JOB_NODELIST     = ${SLURM_JOB_NODELIST}"

# Move to submit directory.
cd ${SLURM_SUBMIT_DIR}

# Location of scratch directory on the compute nodes
scratchlocation=/scratch/users

# Create a user directory if it does not exist
if [ ! -d $scratchlocation/$USER ]; then
    mkdir -vp $scratchlocation/$USER
fi

# Create a temporary directory with a unique identifier associated with your jobid.
TMP_WORK_DIR=$(mktemp -d $scratchlocation/$USER/${SLURM_JOB_NAME}.${SLURM_JOB_ID}.XXXX)
echo "TMP_WORK_DIR           = ${TMP_WORK_DIR}"

# Exit if tmp work dir does not exist.
if [ ! -d ${TMP_WORK_DIR} ]; then
    echo "Temporary scratch directory does not exist ..."
    echo "Something is wrong, contact support."
    exit
fi

# Move to temporary work dir.
cd "${TMP_WORK_DIR}"

# Copy files required for scientific application.
cp ${SLURM_SUBMIT_DIR}/* ${TMP_WORK_DIR}/

# Prevent hyperthreading from wreaking havoc.
export OMP_NUM_THREADS=1
export MKL_NUM_THREADS=1

# Execute calculation
time mpirun -np ${SLURM_NTASKS} gpaw python ice_cohesive_energy.py | tee ice_cohesive_energy.out &
wait

echo ""
echo "Scientific application has finished."
echo " "

# Release user defined signal handler for USR1.
trap - USR1

# Re-create SLURM_SUBMIT_DIR if user has deleted or moved it.
mkdir -vp "${SLURM_SUBMIT_DIR}"

# Copy job output back to submit dir and delete tmp working dir.
rsync -av "${TMP_WORK_DIR}"  "${SLURM_SUBMIT_DIR}"
rsync_code=$?

if [ "$rsync_code" -eq "0" ] ; then
   echo " "
   echo "All files have been moved to the submit directory."
   echo " "
   echo "Cleanup TMP_WORK_DIR ..."
   echo " "
   # Remove TMP_WORK_DIR ...
   if test -z "$SLURM_JOB_NUM_NODES" -o "$SLURM_JOB_NUM_NODES" = "1"; then
      rm -rvf "${TMP_WORK_DIR}"
   fi
else
   echo " "
   echo "rsync exited with exit code ${rsync_code}. Skipping clean up process!"
   echo "The files will remain in on host '$HOSTNAME' at '$TMP_WORK_DIR'"
   echo " "
   echo " "
fi

echo " "
echo "### Exiting"
echo " "
exit
