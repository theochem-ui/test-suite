from testsuite.gpaw.surface_dimer_qmqm import surface_dimer_qmqm
from testsuite.qmmm.surface_dimer_qmmm import surface_dimer_qmmm
import matplotlib.pyplot as plt
import numpy as np

# QM-QM binding energy curve.
qmqm = surface_dimer_qmqm(accurate=True, dmin=2.5, dmax=3.5, dinc=0.1)
qmqm.to_csv("surface_dimer_qmqm.csv")
plt.plot(qmqm["d_OO"], qmqm["E_int"], label="QM-QM")

# QM-QM binding energy curve for reversed system.
qmqm_rev = surface_dimer_qmqm(accurate=True, dmin=2.5, dmax=3.5, dinc=0.1,
                              reverse=True)
qmqm_rev.to_csv("surface_dimer_qmqm_rev.csv")
plt.plot(qmqm_rev["d_OO"], qmqm_rev["E_int"], label="QM-QM (reversed)")

# QM(acceptor)-MM binding energy curve.
qmmm_acc = surface_dimer_qmmm(accurate=True, dmin=2.5, dmax=3.5, dinc=0.1,
                              reverse=False, damping_value=0.82,
                              system=np.ones(1) * 1000)
qmmm_acc.to_csv("surface_dimer_qmmm_acc.csv")
plt.plot(qmmm_acc["d_OO"], qmmm_acc["E_int"], label="QM-MM (acceptor)")

# QM(donor)-MM binding energy curve.
qmmm_don = surface_dimer_qmmm(accurate=True, dmin=2.5, dmax=3.5, dinc=0.1,
                              reverse=True, damping_value=0.82,
                              system=np.ones(1) * 1000)
qmmm_don.to_csv("surface_dimer_qmmm_don.csv")
plt.plot(qmmm_don["d_OO"], qmmm_don["E_int"], label="QM-MM (donor)")

plt.xlabel("$d_\mathrm{O-O}$ / $\mathrm{\AA}$")
plt.ylabel("$E_\mathrm{int}$ / eV")
plt.legend()
plt.savefig("surface_dimer_binding_curve.png")
plt.show()
