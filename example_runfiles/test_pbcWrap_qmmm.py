from testsuite.qmmm.pbcWrap_qmmm import pbcWrap_qmmm
import matplotlib.pyplot as plt
import numpy as np

# Drag one half of a H2O-H2O dimer repeatedly across
# the periodic boundary of a square box with width a.
# Select various energy and force decompositions to
# compare analytical and numerical results.

# 1) Analytical forces.
qmmm_ana = pbcWrap_qmmm(dist=2.8,
                        dinc=0.1,
                        a=10,
                        repeats=2,
                        accurate=True,
                        damping_value=0.82,
                        numerical=False,
                        system=np.ones(1) * 1000)
qmmm_ana.to_csv("qmmm_analytical.csv")
plt.plot(qmmm_ana["z"], qmmm_ana["E_ds"], label="$E_\mathrm{ds} (analytical)$")
plt.plot(qmmm_ana["z"], qmmm_ana["F_ds"], label="$F_\mathrm{ds} (analytical)$")

# 2) Numerical forces.
qmmm_num = pbcWrap_qmmm(dist=2.8,
                        dinc=1.0,
                        a=10,
                        repeats=2,
                        accurate=True,
                        damping_value=0.82,
                        numerical=True,
                        debug_numerical=[1, 1, 1],
                        system=np.ones(1) * 1000)
qmmm_num.to_csv("qmmm_numerical.csv")
plt.plot(qmmm_num["z"], qmmm_num["E_ds"], "--", label="$E_\mathrm{ds} (numerical)$")
plt.plot(qmmm_num["z"], qmmm_num["F_ds"], "--", label="$F_\mathrm{ds} (numerical)$")

# Plot settings.
plt.xlabel("$z$ coordinate / $\mathrm{\AA}$")
plt.ylabel("$E$ or $F$ / eV")
plt.legend()
plt.savefig("pbcWrap_qmmm.png")
plt.show()