import os
import pandas as pd
from gpaw import GPAW
from ase.io import read
from ase.parallel import parprint

def dimer_qmqm(geo=None,
               dmin=2.1,
               dmax=4.0,
               dinc=0.1,
               a=10.,
               xc="PBE",
               accurate=True):
    '''Calculate GPAW water dimer binding energy curve.

    This script performs energy calculation for a water dimer
    at various intermolecular distances to obtain the binding
    energy curve.

    You can pass your own dimer geometry but are expected to
    prepare it such that the atomic sequence is
    [O_1 H_11 H_11 O_2 H_21 H_22].

    Parameters:

    geo : str
        File path to the dimer geometry you want to use.
        Atomic sequence must be [O_1 H_11 H_11 O_2 H_21 H_22].
        Default / None: dimer with experimental geometry.

    dmin : float
        The smallest dimer separation distance to be tested.
        Unit: Angstrom.

    dmax : float
        The largest dimer separation distance to be tested.
        Unit: Angstrom.

    dinc : float
        Dimer distance increment with which to sample the
        binding energy curve. Unit: Angstrom.

    a : float
        The *a* cell length parameter of the cubic box that
        the dimer will be placed in.

    xc : str
        Density functional to be used to calculate the
        interaction energy. Default: PBE.

    accurate : bool
        Switch between low (False) and high (True) accuracy
        settings. Default: True.

    Return values:

    curve : obj
        pandas.DataFrame object holding the binding energy
        curve. Columns:
            d_OO: intermolecular distance in Angstrom.
            E_tot: total interaction energy in eV.

    Usage example:

        >>> from testsuite.gpaw.dimer_qmqm import dimer_qmqm
        >>> df = dimer_qmqm()
        >>> print(df)
    '''
    parprint("\nGPAW QM-QM dimer energy calculation")
    parprint("-----------------------------------\n")

    # Load model system.
    if not geo:
        geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           "../../structures/dimer.xyz")
    atoms = read(geo)
    atoms.pbc = False
    atoms.set_cell([a, a, a])

    # Set dimer to minimum distance given by arg.
    pos = atoms.get_positions()
    dist = pos[3] - pos[0]
    pos[3:] -= dist
    pos[3:] += [dmin, 0, 0]
    atoms.set_positions(pos)
    atoms.center()

    # QM calculator (GPAW) settings.
    logPath = os.path.join(os.getcwd(), "gpaw.out")
    if accurate:
        gpaw_args = dict(convergence={'qpoles': 1e-08,
                                      'dpoles': 1e-08,
                                      'eigenstates': 1e-8,
                                      'density': 1e-8},
                        h=0.175, xc=xc, txt=logPath)
    else:
        gpaw_args = dict(h=0.25, xc=xc, txt=logPath)

    # Initialize results list.
    results = []

    # Calculate number of data points to calculate.
    nInc = int((dmax - dmin) / dinc)

    # Calculate interaction energy at each distance.
    for i in range(0, nInc + 1):
        parprint("Iteration {:d} / {:d}\n".format(i + 1, nInc + 1))

        # Change positions.
        pos = atoms.get_positions()
        pos[3:] += [dinc, 0, 0]
        atoms.set_positions(pos)

        # Calculate O-O distance.
        d_OO = dmin + i * dinc

        # Define fragments.
        mol1 = atoms[:3]
        mol2 = atoms[3:]

        # Calculate total dimer QM energy.
        atoms.calc = GPAW(**gpaw_args)
        E_tot_dimer = atoms.get_potential_energy()

        # Calculate QM energy of each monomer.
        mol1.calc = GPAW(**gpaw_args)
        E_tot_mol1 = mol1.get_potential_energy()
        mol2.calc = GPAW(**gpaw_args)
        E_tot_mol2 = mol2.get_potential_energy()

        # Calculate interaction energy.
        Eint = E_tot_dimer - E_tot_mol1 - E_tot_mol2

        # Energy decomposition into electrostatics,
        # dispersion and repulsion is not available
        # for pure GPAW calculations.

        results.append([d_OO, Eint])

    # Convert results to DataFrame.
    columns = ["d_OO", "E_int"]
    curve = pd.DataFrame(results, columns=columns)

    return curve