import os
import pandas as pd
from gpaw import GPAW
from ase.io import read
from ase.parallel import parprint

def ice_Ecoh_qmqm(xc="PBE", accurate=True):
    '''Calculate the cohesive energy of ice.

    Calculate the cohesive energy of a 96 molecule
    rigid ice arrangement using PBE.

    Parameters:

    xc : str
        Exchange correlation functional used for
        the DFT calculations. Default: PBE.

    accurate : bool
        Switch between low (False) and high (True) accuracy
        settings. Default: True.

    Return values:

    E_coh : float
        The cohesive energy of the system, which is
        E_tot / sum(E_monomers).

    df_mono : obj
        pandas.DataFrame object holding the total QM energy
        values of each water monomer, as indexed by the atomic
        index of the O atoms from the original structure.

    df_layer : obj
        pandas.DataFrame object holding the total QM energy
        values of 1-4 layers of water as well as the atomic
        indices of the O atoms belonging to these layers.

    Usage example:

        >>> from testsuite.gpaw.ice_Ecoh_qmqm import ice_Ecoh_qmqm
        >>> E_coh, df_mono, df_layer = ice_Ecoh_qmqm()
        >>> print("E_coh = ", E_coh)
    '''
    parprint("\nQM ice cohesive energy calculation")
    parprint("----------------------------------\n")

    # Load periodic ice structure.
    # 96 molecules total.
    geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                       "../../structures/ice.xyz")
    atoms = read(geo)

    # Extend z axis to create a slab set PBC to 2D.
    atoms.cell[2] += [0, 0, 15]
    atoms.pbc = [True, True, False]
    atoms.center()

    # QM calculator (GPAW) settings.
    logPath = os.path.join(os.getcwd(), "gpaw.out")
    if accurate:
        gpaw_args = dict(convergence={'qpoles': 1e-08,
                                      'dpoles': 1e-08,
                                      'eigenstates': 1e-8,
                                      'density': 1e-8},
                        h=0.175, xc=xc, txt=logPath)
    else:
        gpaw_args = dict(h=0.25, xc=xc, txt=logPath)

    # Set up list of all O atoms.
    O_list = [atom.index for atom in atoms
              if atom.symbol == "O"]

    # Calculate E_tot for every (isolated) water molecule.
    monomer_energy = []
    for i, O_idx in enumerate(O_list):
        parprint("monomer {:d} / 96\n".format(i + 1))
        mol = atoms[O_idx, O_idx + 1, O_idx + 2]
        mol.calc = GPAW(**gpaw_args)
        e = mol.get_potential_energy()
        monomer_energy.append([O_idx, e])

    df_mono = pd.DataFrame(monomer_energy,
                           columns=["O_idx", "energy"])

    # Calculate QM energy for various splits of the slab.
    layer_energy = []
    for i in range(1, 5):
        parprint("layer {:d} / 4\n".format(i))

        qm_max = i * 24

        # Select z coordinate values of all O atoms.
        O_idx = [atom.index for atom in atoms
                if atom.symbol == "O"]
        pos = atoms[O_idx].get_positions()
        zlist = pos[:,2]

        # Sort by ascending z value, associate with atom index.
        zsort = sorted(list(zip(zlist, O_idx)))

        # Select qm_max water molecules as QM.
        qmidx = []
        for z, idx in zsort[:qm_max]:
            qmidx.extend([idx, idx + 1, idx + 2])

        # Calculate the total QM energy.
        layer = atoms[qmidx]
        layer.calc = GPAW(**gpaw_args)
        e = layer.get_potential_energy()
        layer_energy.append([i, e])

    columns = ["n_layers", "energy"]
    df_layer = pd.DataFrame(layer_energy,
                            columns=columns)

    # Calculate the cohesive energy.
    O_idx, all_E_mono = zip(*monomer_energy)
    E_coh = (layer_energy[-1][1] - sum(all_E_mono)) / 96

    return E_coh, df_mono, df_layer