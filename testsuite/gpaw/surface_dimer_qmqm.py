import os
import pandas as pd
import numpy as np
from ase.io import read
from gpaw import GPAW
from ase.parallel import parprint

def surface_dimer_qmqm(dmin=2.1,
                       dmax=4.0,
                       dinc=0.1,
                       xc="PBE",
                       accurate=True,
                       reverse=False):
    '''Calculate GPAW water dimer binding energy curve.

    This script performs QM energy calculation for a water dimer
    located on top of a graphene surface at various
    intermolecular distances to obtain the binding energy curve.

    Parameters:

    dmin : float
        The smallest dimer separation distance to be tested.
        Unit: Angstrom.

    dmax : float
        The largest dimer separation distance to be tested.
        Unit: Angstrom.

    dinc : float
        Dimer distance increment with which to sample the
        binding energy curve. Unit: Angstrom.

    xc : str
        Density functional to be used to calculate the
        interaction energy. Default: PBE.

    accurate : bool
        Switch between low (False) and high (True) accuracy
        settings. Default: True.

    reverse : bool
        Reverse the orientation of the water dimer on top of
        the surface. False: acceptor is near the surface.
        True: donor is near the surface.

    Return values:

    curve : obj
        pandas.DataFrame object holding the binding energy
        curve. Columns:
            d_OO: intermolecular distance in Angstrom.
            E_tot: total interaction energy in eV.

    Usage example:

        >>> from testsuite.gpaw.surface_dimer_qmqm import surface_dimer_qmqm
        >>> df = surface_dimer_qmqm()
        >>> print(df)
    '''
    parprint("\nQMQM water dimer on graphene surface")
    parprint("------------------------------------\n")

    # Load surface-dimer structure.
    if reverse:
        geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        "../../structures/dimer_surface_reverse.traj")
    else:
        geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        "../../structures/dimer_surface.traj")
    atoms = read(geo)

    # Cell is a bit short in the z direction; enlarge.
    atoms.cell[2][2] += 10

    # Get atomic indices of O atoms.
    O_idx = [atom.index for atom in atoms if atom.symbol == "O"]

    # Define molecule indices.
    idx_mol2 = [O_idx[1], O_idx[1] + 1, O_idx[1] + 2]
    idx_mol1 = [atom.index for atom in atoms if atom.index not in idx_mol2]

    # Set dimer to minimum distance given by arg.
    pos = atoms.get_positions()
    dist = abs(pos[O_idx[1]] - pos[O_idx[0]])
    pos[idx_mol2] -= dist
    pos[idx_mol2] += [0, 0, dmin]
    atoms.set_positions(pos)

    # QM calculator (GPAW) settings.
    logPath = os.path.join(os.getcwd(), "gpaw.out")
    if accurate:
        gpaw_args = dict(convergence={'qpoles': 1e-08,
                                      'dpoles': 1e-08,
                                      'eigenstates': 1e-8,
                                      'density': 1e-8},
                        h=0.175, xc=xc, txt=logPath)
    else:
        gpaw_args = dict(h=0.25, xc=xc, txt=logPath)

    # Initialize results list.
    results = []

    # Calculate number of data points to calculate.
    nInc = int((dmax - dmin) / dinc)

    # Calculate interaction energy at each distance.
    for i in range(0, nInc + 1):
        parprint("Iteration {:d} / {:d}\n".format(i + 1, nInc + 1))

        # Change positions.
        pos = atoms.get_positions()
        pos[idx_mol2] += [0, 0, dinc]
        atoms.set_positions(pos)

        # Calculate O-O distance.
        d_OO = dmin + i * dinc

        # Define fragments.
        mol1 = atoms[idx_mol1]
        mol2 = atoms[idx_mol2]

        # Calculate total QM energy of surface + dimer.
        atoms.calc = GPAW(**gpaw_args)
        E_tot = atoms.get_potential_energy()

        # Calculate QM energy of
        # 1) surface + 1 H2O and
        # 2) second H2O.
        mol1.calc = GPAW(**gpaw_args)
        E_tot_mol1 = mol1.get_potential_energy()
        mol2.calc = GPAW(**gpaw_args)
        E_tot_mol2 = mol2.get_potential_energy()

        # Calculate interaction energy.
        Eint = E_tot - E_tot_mol1 - E_tot_mol2

        # Energy decomposition into electrostatics,
        # dispersion and repulsion is not available
        # for pure GPAW calculations.

        results.append([d_OO, Eint])

    # Convert results to DataFrame.
    columns = ["d_OO", "E_int"]
    curve = pd.DataFrame(results, columns=columns)

    return curve