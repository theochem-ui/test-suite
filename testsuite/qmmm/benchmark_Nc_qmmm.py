import os
import pandas as pd
import numpy as np
from ase_interface import SCME_PS, SCME_QMMM
from ase.calculators.qmmm import EIQMMM, SCMEEmbedding
from ase.io import read
from gpaw import GPAW
from ase import units
from ase.parallel import parprint


def benchmark_Nc_qmmm(h,
                        k,
                        Nc,
                        Nc_outer,
                        # QMMM args
                        damping_value=0.82,
                        # QM args
                        xc="PBE",
                        # MM args
                        numerical=False,
                        debug_numerical=[1, 1, 1],
                        system=np.ones(1) * 1000,
                        para_set='A'):
    parprint("Now processing h=0.2, Nc={:d}, Nc_outer={:d}".format(Nc, Nc_outer)) 
    references = {0.10: {3: -304.512885},
              0.11: {3: -304.506080},
              0.12: {3: -304.498946},
              0.13: {3: -304.482852},
              0.14: {3: -304.471767},
              0.15: {3: -304.43716,
                     5: -305.94750,
                     7: -306.04752},
              0.16: {3: -304.405523},#-308.273136}, for clean graphene
              0.17: {3: -304.349596}, 
              0.18: {3: -304.329212,
                     5: -305.839024,
                     7: -305.939031},
              0.19: {3: -304.247562},
              0.20: {3: -304.140447, #-304.18290, #-308.051318, for clean graphene
                     5: -305.69067,
                     7: -305.79066},
              0.25: {3: -303.18915,
                     5: -304.68624,
                     7: -306.04752}}
    # 1) read the input structure of the Water dimer on B-doped graphene
    geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                       "../../structures/dimer_on_B_graphene.xyz")
    atoms = read(geo)
 
    # 2) Setup QMMM tags for SAFIRES
    # All atoms are asigned to tag = 2
    atoms.set_tags(2)
    # Solute tag = 1
    for atom in atoms:
        if atom.symbol in ["C", "B", "N"]:
            atom.tag = 1
    # MM atoms (last three atoms) get tag = 3
    for atom in atoms:
        if atom.index > 34:
            atom.tag = 3

    # Define indices
    solidx = [atom.index for atom in atoms if atom.tag == 1]
    w_idx = [atom.index for atom in atoms if atom.tag == 2]
    qmidx = [atom.index for atom in atoms if atom.tag in [1,2]]
    mmidx = [atom.index for atom in atoms if atom.tag == 3]


    atoms.pbc = [True, True, False]
    E = []
    d = []
    results = []
    # 3) set paremeters for calculators
    gpaw_args = dict(convergence={'qpoles': 1e-05,
                                'dpoles': 1e-05,
                                'eigenstates': 1e-06,
                                'density': 1e-05},
                    h=h, xc=xc)#, kpts=1)                                                   

    qmcalc = GPAW(txt="gpaw.out", **gpaw_args)
    qmmm_args = {'lrc' : 1000.0, #previously set to 100.0
                'Nc' : np.array([Nc,Nc,0]),
                'Nc_outer': np.array([Nc_outer,Nc_outer,0]),
                'damping_value': damping_value,
                'buffer_region': 1000.0}

    
    mmcalc = SCME_PS(atoms=atoms[mmidx],
                    qm_pbc=atoms.pbc,
                    qmmm=True,
                    NC=np.array([Nc_outer,Nc_outer,0]),
                    dms=False,
                    irigidmolecules=True,
                    numerical=numerical,
                    debug_numerical=debug_numerical,
                    system=system,
                    para_set=para_set) 
    
    atoms.calc = EIQMMM(selection=qmidx,
                    qmcalc=qmcalc,
                    mmcalc=mmcalc,
                    interaction=SCME_QMMM(
                        atoms,
                        w_idx=w_idx,
                        Nc=np.array([Nc_outer,Nc_outer,0]),
                        para_set=para_set,
                        system=system,
                        qmmm=True
                    ),
                    vacuum=None,
                    embedding=SCMEEmbedding(**qmmm_args))
    

    # 4) Calculate
    Eref = -304.140447 #references[h][sum(1)]
    E = atoms.get_potential_energy()
    Ebind = E - Eref

    # 5) Energy decomposition
    E_es_MM = atoms.calc.mmcalc.energy_ES
    E_es_QM = 0.5 * atoms.calc.qmcalc.hamiltonian.e_external * units.Ha
    E_ds = atoms.calc.interaction.energy_DS
    E_rp = atoms.calc.interaction.energy_RP

    # 6) Log results.
    results.append([h, k, Nc, Nc_outer, Eref, E, Ebind, E_es_MM, E_es_QM, E_ds, E_rp])
    columns = ["h", "kpts", "Nc", "Nc_outer", "Eref", "E", "E_bind", "E_es_MM", "E_es_QM", "E_ds", "E_rp"]
    curve = pd.DataFrame(results, columns=columns)

    return curve
