import os
import pandas as pd
import numpy as np
from ase_interface import SCME_PS, SCME_QMMM
from ase.calculators.qmmm import EIQMMM, SCMEEmbedding
from ase.io import read
from ase.parallel import parprint
from ase import units
from gpaw import GPAW

def dimer_qmmm(geo=None,
               dmin=2.1,
               dmax=4.0,
               dinc=0.1,
               a=10.,
               pbc=False,
               # QMMM args
               reverse=False,
               damping_value=0.82,
               # QM args
               xc="PBE",
               accurate=True,
               # MM args
               numerical=False,
               debug_numerical=[1, 1, 1],
               force_select=0,
               system=np.ones(1) * 1000,
               para_set='A'):
    '''Calculate QMMM water dimer binding energy curve.

    This script performs energy calculation for a water dimer
    at various intermolecular distances to obtain the binding
    energy curve.

    You can pass your own dimer geometry but are expected to
    prepare it such that the atomic sequence is
    [O_1 H_11 H_11 O_2 H_21 H_22].

    Parameters:

    geo : str
        File path to the dimer geometry you want to use.
        Atomic sequence must be [O_1 H_11 H_11 O_2 H_21 H_22].
        Default / None: dimer with experimental geometry.

    dmin : float
        The smallest dimer separation distance to be tested.
        Unit: Angstrom.

    dmax : float
        The largest dimer separation distance to be tested.
        Unit: Angstrom.

    dinc : float
        Dimer distance increment with which to sample the
        binding energy curve. Unit: Angstrom.

    a : float
        The *a* cell length parameter of the cubic box that
        the dimer will be placed in.

    pbc : bool
        Switch to turn on or off periodic boundary conditions.

    reverse : bool
        By default, the first dimer (atoms 0, 1, 2) will be
        treated as QM. Activating `reverse` to treat the
        second dimer (atoms 3, 4, 5) as QM.

    damping_value : float
        QMMM damping value tuning the strength of hydrogen
        bonds resulting from the interaction of
        QM oxygen atom lone pair electrons with an SCME proton.

    xc : str
        Density functional to be used to calculate the
        interaction energy. Default: PBE.

    accurate : bool
        Switch between low (False) and high (True) accuracy
        settings. Default: True.

    numerical : bool
        Switch between analytical (False, default) and
        numerical (True) force calculation.

    debug_numerical : list[3]
        Scaling factors for the calculation of numerical
        forces in the sequence
            [electrostatics, dispersion, repulsion].
        Example: to calculate the numerical forces only
        based on the dispersion energy, set
        debug_numerical = [0, 1, 0].

    force_select : int
        Select 0, 1, or 2 to evaluate forces in x, y, or
        z direction. Recommended direction is x since the
        second water molecule is dragged along the z direction
        and interacts with the other molecule along x.

    system : int
        SCME fit parameter set. Default: PBE-SCME values.

    Return values:

    curve : obj
        pandas.DataFrame object holding the binding energy
        curve. Columns:
            d_OO: intermolecular distance in Angstrom.
            E_int: total interaction energy in eV.
            E_es: electrostatic interaction energy in eV.
            E_ds: dispersion interaction energy in eV.
            E_rp: repulsion interaction energy in eV.

    Usage example:

        >>> from testsuite.qmmm.dimer_qmmm import dimer_qmmm
        >>> df = dimer_qmmm()
        >>> print(df)
    '''
    parprint("\nQMMM dimer energy calculation")
    parprint("-----------------------------\n")

    # Load model system.
    if not geo:
        geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           "../../structures/dimer.xyz")
    atoms = read(geo)
    atoms.pbc = pbc
    atoms.set_cell([a, a, a])

    # Set dimer to minimum distance given by arg.
    pos = atoms.get_positions()
    dist = pos[3] - pos[0]
    pos[3:] -= dist
    pos[3:] += [dmin, 0, 0]
    atoms.set_positions(pos)
    atoms.center()

    # QM calculator (GPAW) settings.
    logPath = os.path.join(os.getcwd(), "gpaw.out")
    if accurate:
        gpaw_args = dict(convergence={'qpoles': 1e-08,
                                      'dpoles': 1e-08,
                                      'eigenstates': 1e-8,
                                      'density': 1e-8},
                        h=0.175, xc=xc, txt=logPath)
    else:
        gpaw_args = dict(h=0.25, xc=xc, txt=logPath)

    # Periodic expansion setup.
    Nc = np.array([0, 0, 0])
    Nc_outer = np.array([0, 0, 0])

    # QMMM embedding settings.
    qmmm_args = {'lrc' : 1000.0,
                 'Nc' : Nc,
                 'Nc_outer' : Nc_outer,
                 'damping_value': damping_value,
                 'buffer_region': 1000.0}

    # Initialize results list.
    results = []

    # Calculate number of data points to calculate.
    nInc = int((dmax - dmin) / dinc)

    # Calculate interaction energy at each distance.
    for i in range(0, nInc + 1):
        parprint("Iteration {:d} / {:d}\n".format(i + 1, nInc + 1))

        # Change positions.
        pos = atoms.get_positions()
        pos[:3] -= [dinc, 0, 0]
        atoms.set_positions(pos)

        #from ase.visualize import view
        #view(atoms)

        # Calculate O-O distance.
        d_OO = dmin + i * dinc

        # Assign fragments to QM or MM.
        if reverse:
            qmidx = [3, 4, 5]
            mmidx = [0, 1, 2]
            molQM = atoms[qmidx]
            molMM = atoms[mmidx]
        else:
            qmidx = [0, 1, 2]
            mmidx = [3, 4, 5]
            molQM = atoms[qmidx]
            molMM = atoms[mmidx]

        # Calculate total energy of QM monomer.
        molQM.calc = GPAW(**gpaw_args)
        E_tot_QM = molQM.get_potential_energy()

        # Setup QMMM calculator.
        qmcalc = GPAW(**gpaw_args)
        mmcalc = SCME_PS(atoms=molMM,
                         qm_pbc=atoms.pbc,
                         qmmm=True,
                         NC=Nc_outer,
                         dms=False,
                         irigidmolecules=True,
                         numerical=numerical,
                         debug_numerical=debug_numerical,
                         system=system,
                         para_set=para_set)
        atoms.calc = EIQMMM(selection=qmidx,
                            qmcalc=qmcalc,
                            mmcalc=mmcalc,
                            interaction=SCME_QMMM(
                                atoms,
                                w_idx=qmidx,
                                Nc=Nc_outer,
                                para_set=para_set,
                                system=system,
                                qmmm=True
                            ),
                            vacuum=None,
                            embedding=SCMEEmbedding(**qmmm_args))

        # Calculate total energy and MM energy decomposition.
        E_tot_dimer = atoms.get_potential_energy()

        # Energy decomposition.
        E_es_MM = atoms.calc.mmcalc.energy_ES
        E_es_QM = 0.5 * atoms.calc.qmcalc.hamiltonian.e_external * units.Ha
        E_ds = atoms.calc.interaction.energy_DS
        E_rp = atoms.calc.interaction.energy_RP
        E_int = E_tot_dimer - E_tot_QM

        # Force decomposition.
        F_es_MM = sum(atoms.calc.mmcalc.forces_ES.reshape(-1,3)[:, force_select])
        F_ds = sum(atoms.calc.interaction.forces_DS.reshape(-1,3)[mmidx][:, force_select])
        F_rp = sum(atoms.calc.interaction.forces_RP.reshape(-1,3)[mmidx][:, force_select])

        results.append([d_OO, E_int, E_es_QM, E_es_MM, E_ds, E_rp,
                        F_es_MM, F_ds, F_rp])

    # Log results.
    columns = ["d_OO", "E_int", "E_es_QM", "E_es_MM", "E_ds", "E_rp",
               "F_es_MM", "F_ds", "F_rp"]
    curve = pd.DataFrame(results, columns=columns)

    return curve
