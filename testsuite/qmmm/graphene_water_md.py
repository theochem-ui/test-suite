import os
import numpy as np

from ase import units
from ase.io import Trajectory, read
from ase.constraints import FixAtoms
from ase.md.safires import SAFIRES

from asetools.geotools import make_rattle_tuples
from asetools.safirestools import expand_regions

from gpaw import GPAW
from gpaw.utilities.watermodel import FixBondLengthsWaterModel as FixBondLengths

from ase_interface import SCME_PS, SCME_QMMM
from ase.calculators.qmmm import EIQMMM, SCMEEmbedding
from ase.parallel import parprint

def graphene_water_md(niters=100000,
                      trajfreq=1,
                      nqm=16,
                      seed=42,
                      # QMMM args
                      damping_value=0.82,
                      Nc = np.array([1,1,0]),
                      Nc_outer = np.array([9,9,0]),
                      # QM args
                      xc="PBE",
                      accurate=True,
                      # MM args
                      numerical=False,
                      debug_numerical=[1, 1, 1],
                      system=np.ones(1) * 1000,
                      para_set='A'):
    """Perform QMMM MD on a graphene model with 4 layers of water.

    Run QMMMM molecular dynamics simulations with a graphene model
    system with 4 layers of water molecules. The simulations use
    the SAFIRES boundary method to separate QM and MM molecules.
    Standard MD trajectory and log files are generated.

    WARNING: This script depends on
        ASE Helper Tools (https://bjk24.gitlab.io/asetools)
    to expand the tags for the QM and MM regions.

    Use `nqm` to change how many water molecules will be treated
    with QM, the corresponding number of MM molecules will then
    be 32 - nqm.

    Since the original, pure QM calculations that this can be
    compared to used bond length constraints, this, too, must
    use bond length constraints. The time step value is fixed
    for the same reason.

    The accuracy parameters are chosen to fit the original pure
    QM MD simulations. Use accurate=False for debugging
    purposes only!

    Parameters:

    niters : int
        Number of maximum MD iterations.

    trajfreq : int
        Frequency with which the trajectory file is written.

    nqm : int
        Number of water molecules to be included in the QM
        subsystem. Cannot exceed 31. Recommended to use multiples
        of 8 since this model accomodates 8 water molecules per
        layer.

    damping_value : float
        QMMM damping value tuning the strength of hydrogen
        bonds resulting from the interaction of
        QM oxygen atom lone pair electrons with an SCME proton.

    Nc : list[3]
        Number of copies in each dimension to be taken into
        account for the periodic images on the FINE grid.

    Nc_outer : list[3]
        Number of copies in each dimension to be taken into
        account for the periodic images on the COARSE grid.
        Each Nc_outer entry must be >= entry in Nc.

    xc : str
        Density functional to be used to calculate the
        interaction energy. Default: PBE.

    accurate : bool
        Switch between low (False) and high (True) accuracy
        settings. Default: True.

    system : int
        SCME fit parameter set. Default: PBE-SCME values.

    numerical : bool
        Switch between analytical (False, default) and
        numerical (True) force calculation.

    debug_numerical : list[3]
        Scaling factors for the calculation of numerical
        forces in the sequence
            [electrostatics, dispersion, repulsion].
        Example: to calculate the numerical forces only
        based on the dispersion energy, set
        debug_numerical = [0, 1, 0].

    Usage example:

        >>> from testsuite.qmmm.graphene_water_md import graphene_water_md
        >>> graphene_water_md(niters=10, nqm=8)
    """
    parprint("\nQMMM MD simulation of water on graphene")
    parprint("---------------------------------------\n")

    # Assert nqm parameter.
    assert (nqm > 0 and nqm < 32), f"nqm must be > 0 and < 32! got {nqm}"

    # Read in the model system.
    geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                       "../../structures/graphene_water_4layers.traj")
    atoms = read(geo)
    atoms.pbc = [True, True, False]

    # Reset any constraints.
    atoms.constraints = []

    # Expand QM and MM region and tag according to SAFIRES specs.
    # (Solute = 1 has to be set first, then expand_regions will
    #  tag QM molecules with 2 and MM with 3 automatically.)
    for atom in atoms:
        if atom.symbol == "C":
            atom.tag = 1
    atoms = expand_regions(atoms, nqm=nqm, natoms=3, surface=True)

    # Generate atomic index tuples for each O-H1, O-H2, H1-H2
    # in each water molecule.
    rattle = make_rattle_tuples(atoms, natoms=3, exclude_sym=["C"])
    atoms.constraints.append(FixBondLengths(rattle))

    # Fix the carbon sheet.
    fixatoms = FixAtoms([atom.index for atom in atoms if atom.symbol == "C"])
    atoms.constraints.append(fixatoms)

    # Define index lists.
    w_idx = [atom.index for atom in atoms if atom.tag == 2]
    qmidx = [atom.index for atom in atoms if atom.tag in [1,2]]
    mmidx = [atom.index for atom in atoms if atom.tag == 3]

    # QM calculator settings.
    if accurate:
        h = 0.2
    else:
        h = 0.25
    logPath = os.path.join(os.getcwd(), "gpaw.out")
    gpaw_args = dict(convergence={'qpoles': 1e-05,
                                  'dpoles': 1e-05,
                                  'eigenstates': 1e-08,
                                  'density': 1e-06},
                    h=h, xc=xc, kpts=[1,1,1], txt=logPath,
                    occupations={'name': 'fermi-dirac', 'width': 0.05})

    # QMMM embedding settings.
    qmmm_args = {'lrc' : 1000.0,
                 'Nc' : Nc,
                 'Nc_outer' : Nc_outer,
                 'damping_value': damping_value,
                 'buffer_region': 1000.0}

    # Set up QMMM calculator.
    qmcalc = GPAW(**gpaw_args)
    mmcalc = SCME_PS(atoms=atoms[mmidx],
                     qm_pbc=atoms.pbc,
                     qmmm=True,
                     NC=Nc_outer,
                     dms=False,
                     irigidmolecules=True,
                     system=system,
                     numerical=numerical,
                     debug_numerical=debug_numerical,
                     para_set=para_set)
    atoms.calc = EIQMMM(selection=qmidx,
                        qmcalc=qmcalc,
                        mmcalc=mmcalc,
                        interaction=SCME_QMMM(
                            atoms,
                            w_idx=w_idx,
                            Nc=Nc_outer,
                            para_set=para_set,
                            system=system,
                            qmmm=True),
                        vacuum=None,
                        embedding=SCMEEmbedding(**qmmm_args))

    # Set up SAFIRES dynamics.
    md = SAFIRES(atoms = atoms,
                 timestep = 1.0 * units.fs,
                 temperature_K = 300,
                 friction=0.02,
                 logfile='md.log',
                 rng = np.random.default_rng(seed),
                 natoms=3,
                 surface=True,
                 debug=True)

    # Define and attach trajectory object.
    traj = Trajectory('md.traj', 'w', atoms)
    md.attach(traj.write, interval=trajfreq)

    # Run the dynamics.
    md.run(niters)
