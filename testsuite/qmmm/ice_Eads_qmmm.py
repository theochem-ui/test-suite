import os
import numpy as np
import pandas as pd
from gpaw import GPAW
from ase_interface import SCME_PS
from ase_interface import SCME_QMMM
from ase.calculators.qmmm import EIQMMM, SCMEEmbedding
from ase.io import read
from ase.parallel import parprint

def ice_Eads_qmmm(nqm=1,
                  seed=42,
                  nrand=10,
                  xc="PBE",
                  accurate=True,
                  damping_value=0.82,
                  Nc=np.array([1, 1, 0]),
                  Nc_outer=np.array([2, 2, 0]),
                  system=np.ones(1) * 1000,
                  para_set='A'):
    '''Calculate the adsorption energy of ice.

    Calculate the adsorption energy of `nqm` QM water
    molecules in the first and second layer of a
    96 molecule ice slab otherwise described with MM.

    Parameters:

    nqm : int
        Number of water molecules in the currently inspected
        layer that should be treated as QM. Range: 1-23.

    seed : int
        Random seed for numpy.random. Get reproducible
        randomness. Have your cake and eat it, too. Wow.

    nrand : int
        Set how often `nqm` QM molecules will be randomly
        assigned in each the layers and the adsorption
        energy calculated.

    xc : str
        Exchange correlation functional used for
        the DFT calculations. Default: PBE.

    accurate : bool
        Switch between low (False) and high (True) accuracy
        settings. Default: True.

    damping_value : float
        QMMM damping value tuning the strength of hydrogen
        bonds resulting from the interaction of
        QM oxygen atom lone pair electrons with an SCME proton.

    Nc : list[3]
        Number of copies in each dimension to be taken into
        account for the periodic images on the FINE grid.

    Nc_outer : list[3]
        Number of copies in each dimension to be taken into
        account for the periodic images on the COARSE grid.
        Each Nc_outer entry must be >= entry in Nc.

    system : int
        SCME fit parameter set. Default: PBE-SCME values.

    Return values:

    df_layer : obj
        pandas.DataFrame object holding the results.
        Columns:
            N_QM: number of QM molecules.
            E_tot: total QMMM energy.

    Usage example:

        >>> from testsuite.qmmm.ice_Eads_qmmm import ice_Eads_qmmm
        >>> df = ice_Eads_qmmm()
        >>> print(df)
    '''
    parprint("\nQMMM ice adsorption energy calculation")
    parprint("------------------------------------\n")

    # Assert `nqm`.
    assert (nqm < 24 and nqm > 0), 'nqm parameter must be 0 < nqm < 24!'

    # Assert `seed`.
    assert (seed != 69 and seed != 420), 'don\'t let your dreams be memes!'

    # Load periodic ice structure.
    # 96 molecules total.
    geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                       "../../structures/ice.xyz")
    atoms = read(geo)

    # Extend z axis to create a slab set PBC to 2D.
    atoms.cell[2] += [0, 0, 15]
    atoms.pbc = [True, True, False]
    atoms.center()

    # QM calculator (GPAW) settings.
    logPath = os.path.join(os.getcwd(), "gpaw.out")
    if accurate:
        gpaw_args = dict(convergence={'qpoles': 1e-08,
                                      'dpoles': 1e-08,
                                      'eigenstates': 1e-8,
                                      'density': 1e-8},
                        h=0.175, xc=xc, txt=logPath)
    else:
        gpaw_args = dict(h=0.25, xc=xc, txt=logPath)

    # Build a library of QM monomer energy results which
    # can be addressed by the atomic index of the O atom.
    parprint("Calculating monomers ...")
    E_mono = {}
    O_list = [atom.index for atom in atoms if atom.symbol == "O"]
    for i, O_idx in enumerate(O_list):
        parprint("Monomer {:d} / {:d}".format(i + 1, len(O_list)))
        monomer = atoms[O_idx, O_idx + 1, O_idx + 2]
        monomer.calc = GPAW(**gpaw_args)
        e = monomer.get_potential_energy()
        parprint("e = ", e)
        E_mono.update({O_idx: e})

    parprint("E_mono = ", E_mono)

    # Select z coordinate values of all O atoms.
    O_idx = [atom.index for atom in atoms
            if atom.symbol == "O"]
    pos = atoms.get_positions()[O_idx]
    zlist = pos[:,2]

    # Sort by ascending z value, associate with atom index.
    zsort = sorted(list(zip(zlist, O_idx)))

    # Randomly assign nqm molecules in the first two layers
    # as QM, everything else as MM. Calculate adsorption energy.
    results = []
    rng = np.random.default_rng(seed)
    for layer in range(0, 2):

        parprint("\nlayer: ", (layer + 1))

        for n in range(nrand):

            parprint("Random Iteration ", (n + 1))

            # Initialize QM and MM arrays.
            qmidx = []
            mmidx = []

            # Select nqm molecules and assign as QM from the
            # currently selected layer.
            lower_limit = layer * 24
            upper_limit = (layer + 1) * 24
            for j in range(nqm):
                randint = rng.integers(lower_limit, high=upper_limit)
                if randint not in qmidx:
                    tmpidx = zsort[randint][1]
                    qmidx.extend([tmpidx, tmpidx + 1, tmpidx + 2])
            mmidx = [atom.index for atom in atoms if atom.index not in qmidx]

            # Initialize QM (GPAW) calculator.
            qmcalc = GPAW(**gpaw_args)

            # MM calculator (SCME) settings.
            mmcalc = SCME_PS(atoms=atoms[mmidx],
                            qm_pbc=atoms.pbc,
                            qmmm=True,
                            NC=Nc_outer,
                            dms=False,
                            irigidmolecules=True,
                            system=system,
                            para_set=para_set)

            # QMMM embedding settings.
            qmmm_args = {'lrc' : 1000.0,
                        'Nc' : Nc,
                        'Nc_outer' : Nc_outer,
                        'damping_value': damping_value,
                        'buffer_region': 1000.0}

            # Define QMMM calculator.
            atoms.calc = EIQMMM(selection=qmidx,
                                qmcalc=qmcalc,
                                mmcalc=mmcalc,
                                interaction=SCME_QMMM(
                                    atoms,
                                    w_idx=qmidx,
                                    Nc=Nc_outer,
                                    para_set=para_set,
                                    system=system,
                                    qmmm=True
                                ),
                                vacuum=None,
                                embedding=SCMEEmbedding(**qmmm_args))

            # Calculate total QMMM energy.
            E_qmmm = atoms.get_potential_energy()

            # Sum over all QM monomer energy values.
            O_select = [idx for idx in O_list if idx in qmidx]
            E_mono_sum = 0.0
            for idx in O_select:
                E_mono_sum += E_mono[idx]

            # Calculate total MM energy.
            parprint("  MM calculation ...")
            mmatoms = atoms[mmidx]
            mmatoms.calc = SCME_PS(atoms=mmatoms,
                                qmmm=False,
                                NC=Nc_outer,
                                dms=False,
                                irigidmolecules=True,
                                system=system,
                                para_set=para_set)
            E_mm = mmatoms.get_potential_energy()

            # Calculate adsorption energy.
            E_ads = (E_qmmm - E_mm - E_mono_sum) / nqm

            results.append([layer, nqm, E_ads, E_qmmm, E_mm, E_mono_sum])

    return pd.DataFrame(results, columns=["layer", "N_QM",
                                          "E_coh", "E_QMMM",
                                          "E_MM",  "E_QM_mono_sum"])
