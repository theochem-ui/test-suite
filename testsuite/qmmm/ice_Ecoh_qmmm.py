import os
import numpy as np
import pandas as pd
from gpaw import GPAW
from ase_interface import SCME_PS
from ase_interface import SCME_QMMM
from ase.calculators.qmmm import EIQMMM, SCMEEmbedding
from ase.io import read
from ase.parallel import parprint

def ice_Ecoh_qmmm(nqm=48,
                  nrand=10,
                  seed=42,
                  xc="PBE",
                  accurate=True,
                  damping_value=0.82,
                  Nc=np.array([1, 1, 1]),
                  Nc_outer=np.array([2, 2, 2]),
                  system=np.ones(1) * 1000,
                  para_set='A'):
    '''Calculate the cohesive energy of ice with random QM/MM split.

    This script calculates the cohesive energy of an ice crystal
    with 96 water molecules where `nqm` molecules are treated as
    qm and `96 - nqm` are treated as MM. The assignment of QM and
    MM is chosen at random, and the random treatment is repeated
    `nrand` times to see if different assignments result in the
    same total energy response of the QMMM interface.

    Parameters:

    nqm : int
        Number of water molecules (out of 96) which should be
        treated as QM. Max. 95.

    nrand : int
        Number of randomized cohesive energy calculations that
        should be performed to the consistency of the results.

    seed : int
        Random seed for numpy.random. Get reproducible
        randomness. Have your cake and eat it, too. Wow.

    xc : str
        Exchange correlation functional used for
        the DFT calculations. Default: PBE.

    accurate : bool
        Switch between low (False) and high (True) accuracy
        settings. Default: True.

    damping_value : float
        QMMM damping value tuning the strength of hydrogen
        bonds resulting from the interaction of
        QM oxygen atom lone pair electrons with an SCME proton.

    Nc : list[3]
        Number of copies in each dimension to be taken into
        account for the periodic images on the FINE grid.

    Nc_outer : list[3]
        Number of copies in each dimension to be taken into
        account for the periodic images on the COARSE grid.
        Each Nc_outer entry must be >= entry in Nc.

    system : int
        SCME fit parameter set. Default: PBE-SCME values.

    Return values:

    df_layer : obj
        pandas.DataFrame object holding the results.
        Columns:
            N_QM: number of QM molecules.
            E_tot: total QMMM energy.

    Usage example:

        >>> from testsuite.qmmm.ice_Ecoh_qmmm import ice_Ecoh_qmmm
        >>> df = ice_Ecoh_qmmm()
        >>> print(df)
    '''
    parprint("\nQMMM randomIce cohesive energy calculation")
    parprint("------------------------------------------\n")

    # Assert `nqm`.
    assert (nqm < 96), 'nqm parameter must be < 96!'

    # Assert `seed`.
    assert (seed != 69 and seed != 420), 'don\'t let your dreams be memes!'

    # Load periodic ice structure.
    # 96 molecules total.
    geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                       "../../structures/ice.xyz")
    atoms = read(geo)

    # QM calculator (GPAW) settings.
    logPath = os.path.join(os.getcwd(), "gpaw.out")
    if accurate:
        gpaw_args = dict(convergence={'qpoles': 1e-08,
                                      'dpoles': 1e-08,
                                      'eigenstates': 1e-8,
                                      'density': 1e-8},
                        h=0.175, xc=xc, txt=logPath)
    else:
        gpaw_args = dict(h=0.25, xc=xc, txt=logPath)

    # Build a library of QM monomer energy results which
    # can be addressed by the atomic index of the O atom.
    parprint("Calculating monomers ...")
    E_mono = {}
    O_list = [atom.index for atom in atoms if atom.symbol == "O"]
    for i, O_idx in enumerate(O_list):
        parprint("Monomer {:d} / {:d}".format(i + 1, len(O_list)))
        monomer = atoms[O_idx, O_idx + 1, O_idx + 2]
        monomer.calc = GPAW(**gpaw_args)
        e = monomer.get_potential_energy()
        parprint("e = ", e)
        E_mono.update({O_idx: e})

    parprint("E_mono = ", E_mono)

    # For a total of nrand iterations ...
    results = []
    rng = np.random.default_rng(seed)
    for i in range(nrand):
        # ... assign QM and MM molecules at random.
        parprint("Random iteration", i)
        qmidx =[]
        while len(qmidx) < nqm * 3:
            randint = rng.integers(0, high=len(O_list))
            O_idx = O_list[randint]
            if O_idx not in qmidx:
                qmidx.extend([O_idx, O_idx + 1, O_idx + 2])
        mmidx = [atom.index for atom in atoms if atom.index not in qmidx]

        # Initialize QM (GPAW) calculator.
        qmcalc = GPAW(**gpaw_args)

        # MM calculator (SCME) settings.
        mmcalc = SCME_PS(atoms=atoms[mmidx],
                         qm_pbc=atoms.pbc,
                         qmmm=True,
                         NC=Nc_outer,
                         dms=False,
                         irigidmolecules=True,
                         system=system,
                         para_set=para_set)

        # QMMM embedding settings.
        qmmm_args = {'lrc' : 1000.0,
                     'Nc' : Nc,
                     'Nc_outer' : Nc_outer,
                     'damping_value': damping_value,
                     'buffer_region': 1000.0}

        # Define QMMM calculator.
        atoms.calc = EIQMMM(selection=qmidx,
                            qmcalc=qmcalc,
                            mmcalc=mmcalc,
                            interaction=SCME_QMMM(
                                atoms,
                                w_idx=qmidx,
                                Nc=Nc_outer,
                                para_set=para_set,
                                system=system,
                                qmmm=True
                            ),
                            vacuum=None,
                            embedding=SCMEEmbedding(**qmmm_args))

        # Calculate total QMMM energy.
        parprint("  QMMM calculation ...")
        E_qmmm = atoms.get_potential_energy()

        # Sum over all QM monomer energy values.
        O_select = [idx for idx in O_list if idx in qmidx]
        E_mono_sum = 0.0
        for idx in O_select:
            E_mono_sum += E_mono[idx]

        # Calculate cohesive energy.
        E_coh = (E_qmmm - E_mono_sum) / len(O_list)

        # Save results.
        results.append([E_coh, E_qmmm, E_mono_sum])

    # Convert results to DataFrame and return.
    columns = ["E_coh", "E_qmmm", "E_mono_sum"]
    df = pd.DataFrame(results, columns=columns)

    return df
