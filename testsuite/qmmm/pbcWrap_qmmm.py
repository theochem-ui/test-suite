import os
import pandas as pd
import numpy as np
from ase_interface import SCME_PS, SCME_QMMM
from ase.calculators.qmmm import EIQMMM, SCMEEmbedding
from ase.io import read
from ase.parallel import parprint
from ase import units
from gpaw import GPAW

def pbcWrap_qmmm(dist=2.8,
                 dinc=0.1,
                 a=10.,
                 repeats=2,
                 reverse=False,
                 # QMMM args
                 damping_value=0.82,
                 # QM args
                 xc="PBE",
                 accurate=True,
                 # MM args
                 numerical=False,
                 debug_numerical=[1, 1, 1],
                 force_select=0,
                 system=np.ones(1) * 1000,
                 para_set='A'):
    '''Test PBC wrapping in a QMMM scenario.

    This script performs energy calculation for a water dimer
    at various intermolecular distances to obtain the binding
    energy curve while crossing the periodic boundaries.

    Important note on forces: since forces in the total system
    always sum up to zero, to evaluate them, we look at the forces
    only on molecule (the SCME MM molecule) and only along one
    axis; x, y, or z as selected by force_select = 0, 1, or 2.

    Parameters:

    dist : float
        Distance of the oxygen atoms of the dimer to be tested.
        One of the molecules will be dragged in a perpendicular
        direction to the axis that connects the dimer, the second
        molecule will therefore pass the first repeatedly at this
        distance when being dragged across the periodic boundary
        multiple times. Unit: Angstrom.

    dinc : float
        Distance increment with which to sample the
        energy landscape. Unit: Angstrom.

    a : float
        The *a* cell length parameter of the cubic box that
        the dimer will be placed in.

    repeats : int
        Set how often the molecule will be dragged through the
        entire cell. (total distance = repeats * a.)

    reverse : bool
        Flip description of QM and MM between the two molecules.

    damping_value : float
        QMMM damping value tuning the strength of hydrogen
        bonds resulting from the interaction of
        QM oxygen atom lone pair electrons with an SCME proton.

    xc : str
        Density functional to be used to calculate the
        interaction energy. Default: PBE.

    accurate : bool
        Switch between low (False) and high (True) accuracy
        settings. Default: True.

    numerical : bool
        Switch between analytical (False, default) and
        numerical (True) force calculation.

    debug_numerical : list[3]
        Scaling factors for the calculation of numerical
        forces in the sequence
            [electrostatics, dispersion, repulsion].
        Example: to calculate the numerical forces only
        based on the dispersion energy, set
        debug_numerical = [0, 1, 0].

    force_select : int
        Select 0, 1, or 2 to evaluate forces in x, y, or
        z direction. Recommended direction is x since the
        second water molecule is dragged along the z direction
        and interacts with the other molecule along x.

    system : int
        SCME fit parameter set. Default: PBE-SCME values.

    Return values:

    curve : obj
        pandas.DataFrame object holding the binding energy
        curve. Columns:
            d_OO: intermolecular distance in Angstrom.
            E_int: total interaction energy in eV.
            E_es: electrostatic interaction energy in eV.
            E_ds: dispersion interaction energy in eV.
            E_rp: repulsion interaction energy in eV.

    Usage example:

        >>> from testsuite.qmmm.pbcWrap_qmmm import pbcWrap_qmmm
        >>> df = pbcWrap_qmmm()
        >>> print(df)
    '''
    parprint("\nQMMM PBC wrapping test calculation")
    parprint("----------------------------------\n")

    # Load model system.
    geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                       "../../structures/dimer.xyz")
    atoms = read(geo)
    atoms.pbc = True
    atoms.set_cell([a, a, a])

    # Set dimer to minimum distance given by arg.
    pos = atoms.get_positions()
    sep = pos[3] - pos[0]
    pos[3:] -= sep
    pos[3:] += [dist, 0, 0]
    atoms.set_positions(pos)
    atoms.center()

    # Periodic expansion setup.
    Nc = np.array([1, 1, 1])
    Nc_outer = np.array([1, 1, 1])

    # QM calculator (GPAW) settings.
    logPath = os.path.join(os.getcwd(), "gpaw.out")
    if accurate:
        gpaw_args = dict(convergence={'qpoles': 1e-08,
                                      'dpoles': 1e-08,
                                      'eigenstates': 1e-8,
                                      'density': 1e-8},
                        h=0.175, xc=xc, txt=logPath)
    else:
        gpaw_args = dict(h=0.25, xc=xc, txt=logPath)

    # QMMM embedding settings.
    qmmm_args = {'lrc' : 1000.0,
                 'Nc' : Nc,
                 'Nc_outer' : Nc_outer,
                 'damping_value': damping_value,
                 'buffer_region': 1000.0}

    # Initialize results list.
    results = []

    # Calculate number of data points to calculate.
    nInc = int(repeats * a / dinc)

    # Calculate interaction energy at each distance.
    for i in range(0, nInc + 1):
        parprint("Iteration {:d} / {:d}\n".format(i + 1, nInc + 1))

        # Change positions.
        pos = atoms.get_positions()
        pos[3:] += [0, 0, dinc]
        atoms.set_positions(pos)

        # Log z coordinate of molecule.
        z = pos[3][2]

        # Assign fragments to QM or MM.
        if not reverse:
            qmidx = [0, 1, 2]
            mmidx = [3, 4, 5]
        else:
            qmidx = [3, 4, 5]
            mmidx = [0, 1, 2]
        molQM = atoms[qmidx]
        molMM = atoms[mmidx]

        # Calculate total energy of QM monomer.
        molQM.calc = GPAW(**gpaw_args)
        E_tot_QM = molQM.get_potential_energy()

        # Setup QMMM calculator.
        qmcalc = GPAW(**gpaw_args)
        mmcalc = SCME_PS(atoms=molMM,
                         qm_pbc=atoms.pbc,
                         qmmm=True,
                         NC=Nc_outer,
                         dms=False,
                         irigidmolecules=True,
                         system=system,
                         para_set=para_set)
        atoms.calc = EIQMMM(selection=qmidx,
                            qmcalc=qmcalc,
                            mmcalc=mmcalc,
                            interaction=SCME_QMMM(
                                atoms,
                                w_idx=qmidx,
                                Nc=Nc_outer,
                                numerical=numerical,
                                debug_numerical=debug_numerical,
                                system=system,
                                para_set=para_set,
                                qmmm=True
                            ),
                            vacuum=None,
                            embedding=SCMEEmbedding(**qmmm_args))

        # Calculate total energy and interaction energy.
        E_tot_dimer = atoms.get_potential_energy()
        E_int = E_tot_dimer - E_tot_QM

        # Energy decomposition.
        E_es_MM = atoms.calc.mmcalc.energy_ES
        E_es_QM = 0.5 * atoms.calc.qmcalc.hamiltonian.e_external * units.Ha
        E_ds = atoms.calc.interaction.energy_DS
        E_rp = atoms.calc.interaction.energy_RP

        # Force decomposition.
        F_es_MM = sum(atoms.calc.mmcalc.forces_ES.reshape(-1,3)[:, force_select])
        F_ds = sum(atoms.calc.interaction.forces_DS.reshape(-1,3)[mmidx][:, force_select])
        F_rp = sum(atoms.calc.interaction.forces_RP.reshape(-1,3)[mmidx][:, force_select])

        results.append([z, E_int, E_es_QM, E_es_MM, E_ds, E_rp,
                        F_es_MM, F_ds, F_rp])

    # Log results.
    columns = ["z", "E_int", "E_es_QM", "E_es_MM", "E_ds", "E_rp",
               "F_es_MM", "F_ds", "F_rp"]
    curve = pd.DataFrame(results, columns=columns)

    return curve
