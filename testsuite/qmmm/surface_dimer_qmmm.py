import os
import pandas as pd
import numpy as np
from ase_interface import SCME_PS, SCME_QMMM
from ase.calculators.qmmm import EIQMMM, SCMEEmbedding
from ase.io import read
from gpaw import GPAW
from ase.parallel import parprint
from ase import units

def surface_dimer_qmmm(dmin=2.1,
                       dmax=4.0,
                       dinc=0.1,
                       # QMMM args
                       reverse=False,
                       damping_value=0.82,
                       Nc=np.array([1, 1, 0]),
                       Nc_outer=np.array([2, 2, 0]),
                       # QM args
                       xc="PBE",
                       accurate=True,
                       # MM args
                       numerical=False,
                       debug_numerical=[1, 1, 1],
                       system=np.ones(1) * 1000,
                       para_set='A'):
    '''Calculate QMMM water dimer on graphene binding energy curve.

    This script performs QMMM energy calculations for a water dimer
    located on top of a graphene surface at various
    intermolecular distances to obtain the binding energy curve.

    Parameters:

    dmin : float
        The smallest dimer separation distance to be tested.
        Unit: Angstrom.

    dmax : float
        The largest dimer separation distance to be tested.
        Unit: Angstrom.

    dinc : float
        Dimer distance increment with which to sample the
        binding energy curve. Unit: Angstrom.

    reverse : bool
        Reverse the orientation of the water dimer on top of
        the surface. False: acceptor is near the surface.
        True: donor is near the surface.

    damping_value : float
        QMMM damping value tuning the strength of hydrogen
        bonds resulting from the interaction of
        QM oxygen atom lone pair electrons with an SCME proton.

    Nc : list[3]
        Number of copies in each dimension to be taken into
        account for the periodic images on the FINE grid.

    Nc_outer : list[3]
        Number of copies in each dimension to be taken into
        account for the periodic images on the COARSE grid.
        Each Nc_outer entry must be >= entry in Nc.

    xc : str
        Density functional to be used to calculate the
        interaction energy. Default: PBE.

    accurate : bool
        Switch between low (False) and high (True) accuracy
        settings. Default: True.

    numerical : bool
        Switch between analytical (False, default) and
        numerical (True) force calculation.

    debug_numerical : list[3]
        Scaling factors for the calculation of numerical
        forces in the sequence
            [electrostatics, dispersion, repulsion].
        Example: to calculate the numerical forces only
        based on the dispersion energy, set
        debug_numerical = [0, 1, 0].

    system : int
        SCME fit parameter set. Default: PBE-SCME values.

    Return values:

    curve : obj
        pandas.DataFrame object holding the binding energy
        curve. Columns:
            d_OO: intermolecular distance in Angstrom.
            E_tot: total interaction energy in eV.

    Usage example:

        >>> from testsuite.qmmm.surface_dimer_qmmm import surface_dimer_qmmm
        >>> df = surface_dimer_qmmm()
        >>> print(df)
    '''
    parprint("\nQMMM water dimer on graphene surface")
    parprint("------------------------------------\n")

    # Load surface-dimer structure.
    if reverse:
        geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        "../../structures/dimer_surface_reverse.traj")
    else:
        geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                    "../../structures/dimer_surface.traj")
    atoms = read(geo)

    # Cell is too small in the z direction; enlarge.
    atoms.cell[2][2] += 10

    # Get atomic indices of O atoms.
    O_idx = [atom.index for atom in atoms if atom.symbol == "O"]

    # Define molecule indices.
    mmidx = [O_idx[1], O_idx[1] + 1, O_idx[1] + 2]
    qmidx = [atom.index for atom in atoms if atom.index not in mmidx]
    w_idx = [O_idx[0], O_idx[0] + 1, O_idx[0] + 2]

    # Set dimer to minimum distance given by arg.
    pos = atoms.get_positions()
    dist = abs(pos[O_idx[1]] - pos[O_idx[0]])
    pos[mmidx] -= dist
    pos[mmidx] += [0, 0, dmin]
    atoms.set_positions(pos)

    # QM calculator (GPAW) settings.
    logPath = os.path.join(os.getcwd(), "gpaw.out")
    if accurate:
        gpaw_args = dict(convergence={'qpoles': 1e-08,
                                      'dpoles': 1e-08,
                                      'eigenstates': 1e-8,
                                      'density': 1e-8},
                        h=0.175, xc=xc, txt=logPath)
    else:
        gpaw_args = dict(h=0.25, xc=xc, txt=logPath)

    # QMMM embedding settings.
    qmmm_args = {'lrc' : 1000.0,
                 'Nc' : Nc,
                 'Nc_outer' : Nc_outer,
                 'damping_value': damping_value,
                 'buffer_region': 1000.0}

    # Initialize results list.
    results = []

    # Calculate number of data points to calculate.
    nInc = int((dmax - dmin) / dinc)

    # Calculate interaction energy at each distance.
    for i in range(0, nInc + 1):
        parprint("Iteration {:d} / {:d}\n".format(i + 1, nInc + 1))

        # Change positions.
        pos = atoms.get_positions()
        pos[mmidx] += [0, 0, dinc]
        atoms.set_positions(pos)

        # Calculate O-O distance.
        d_OO = dmin + i * dinc

        # Calculate total energy of QM monomer.
        atomsQM = atoms[qmidx]
        atomsQM.calc = GPAW(**gpaw_args)
        E_tot_QM = atomsQM.get_potential_energy()

        # Setup QMMM calculator.
        qmcalc = GPAW(**gpaw_args)
        mmcalc = SCME_PS(atoms=atoms[mmidx],
                         qm_pbc=atoms.pbc,
                         qmmm=True,
                         NC=Nc_outer,
                         dms=False,
                         irigidmolecules=True,
                         numerical=numerical,
                         debug_numerical=debug_numerical,
                         system=system,
                         rc_Elec=1000.0,
                         para_set=para_set)
        atoms.calc = EIQMMM(selection=qmidx,
                            qmcalc=qmcalc,
                            mmcalc=mmcalc,
                            interaction=SCME_QMMM(
                                atoms,
                                w_idx=w_idx,
                                Nc=Nc_outer,
                                para_set=para_set,
                                system=system,
                                qmmm=True
                            ),
                            vacuum=None,
                            embedding=SCMEEmbedding(**qmmm_args))

        # Calculate total energy and MM energy decomposition.
        E_tot_dimer = atoms.get_potential_energy()
        E_int = E_tot_dimer - E_tot_QM

        # Energy decomposition.
        E_es_MM = atoms.calc.mmcalc.energy_ES
        E_es_QM = 0.5 * atoms.calc.qmcalc.hamiltonian.e_external * units.Ha
        E_ds = atoms.calc.interaction.energy_DS
        E_rp = atoms.calc.interaction.energy_RP

        results.append([d_OO, E_int, E_es_QM, E_es_MM, E_ds, E_rp])

    # Log results.
    columns = ["d_OO", "E_int", "E_es_QM", "E_es_MM", "E_ds", "E_rp"]
    curve = pd.DataFrame(results, columns=columns)

    return curve
