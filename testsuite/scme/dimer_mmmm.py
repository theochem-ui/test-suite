import os
import pandas as pd
import numpy as np
from ase_interface import SCME_PS
from ase.io import read
from ase.parallel import parprint

def dimer_mmmm(geo=None,
               dmin=2.1,
               dmax=4.0,
               dinc=0.1,
               a=10.,
               system=np.ones(1) * 1000,
               para_set='A'):
    '''Calculate SCME water dimer binding energy curve.

    This script performs energy calculation for a water dimer
    at various intermolecular distances to obtain the binding
    energy curve.

    You can pass your own dimer geometry but are expected to
    prepare it such that the atomic sequence is
    [O_1 H_11 H_11 O_2 H_21 H_22].

    Parameters:

    geo : str
        File path to the dimer geometry you want to use.
        Atomic sequence must be [O_1 H_11 H_11 O_2 H_21 H_22].
        Default / None: dimer with experimental geometry.

    dmin : float
        The smallest dimer separation distance to be tested.
        Unit: Angstrom.

    dmax : float
        The largest dimer separation distance to be tested.
        Unit: Angstrom.

    dinc : float
        Dimer distance increment with which to sample the
        binding energy curve. Unit: Angstrom.

    a : float
        The *a* cell length parameter of the cubic box that
        the dimer will be placed in.

    system : int
        SCME fit parameter set. Default: PBE-SCME values.

    Return values:

    curve : obj
        pandas.DataFrame object holding the binding energy
        curve. Columns:
            d_OO: intermolecular distance in Angstrom.
            E_int: total interaction energy in eV.
            E_es: electrostatic interaction energy in eV.
            E_ds: dispersion interaction energy in eV.
            E_rp: repulsion interaction energy in eV.

    Usage example:

        >>> from testsuite.scme.dimer_mmmm import dimer_mmmm
        >>> df = dimer_mmmm()
        >>> print(df)
    '''
    parprint("\nSCME MM-MM dimer energy calculation")
    parprint("-----------------------------------\n")

    # Load model system.
    if not geo:
        geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           "../../structures/dimer.xyz")
    atoms = read(geo)
    atoms.pbc = False
    atoms.set_cell([a, a, a])

    # Set dimer to minimum distance given by arg.
    pos = atoms.get_positions()
    dist = pos[3] - pos[0]
    pos[3:] -= dist
    pos[3:] += [dmin, 0, 0]
    atoms.set_positions(pos)
    atoms.center()

    # Periodic expansion setup.
    Nc_outer = np.array([0, 0, 0])

    # Initialize results list.
    results = []

    # Calculate number of data points to calculate.
    nInc = int((dmax - dmin) / dinc)

    # Calculate interaction energy at each distance.
    for i in range(0, nInc + 1):
        parprint("Iteration {:d} / {:d}\n".format(i + 1, nInc + 1))

        # Change positions.
        pos = atoms.get_positions()
        pos[3:] += [dinc, 0, 0]
        atoms.set_positions(pos)

        # Calculate O-O distance.
        d_OO = dmin + i * dinc

        # Calculate MM-MM interaction energy.
        mmcalc = SCME_PS(atoms=atoms,
                            qm_pbc=atoms.pbc,
                            qmmm=False,
                            NC=Nc_outer,
                            dms=False,
                            irigidmolecules=True,
                            system=system,
                            para_set=para_set)
        atoms.calc = mmcalc

        # Get energy values.
        E_int = atoms.get_potential_energy()
        E_es = atoms.calc.energy_ES
        E_ds = atoms.calc.energy_DS
        E_rp = atoms.calc.energy_RP

        # The total energy and the total interaction energy
        # are equivalent because the total energy of each
        # single rigid SCME water molecule is zero.

        results.append([d_OO, E_int, E_es, E_ds, E_rp])

    # Convert results to DataFrame.
    columns = ["d_OO", "E_int", "E_es", "E_ds", "E_rp"]
    curve = pd.DataFrame(results, columns=columns)

    return curve
