import os
import numpy as np
from ase_interface import SCME_PS
from ase.io import read
from ase.parallel import parprint

def ice_Ecoh_mmmm(Nc=[1, 1, 0],
                  system=np.ones(1) * 1000,
                  para_set='A'):
    '''Calculate the MM cohesive energy of ice.

    Calculate the cohesive energy of a 96 molecule
    rigid ice arrangement using the SCME function.

    Parameters:

    Nc : list
        Number of copies in each dimension to be taken
        into account for the periodic images.

    te, td, Ar, Br, Cr, rc_Core, rc_Disp : float
        SCME fit parameter. Default: PBE-SCME values.

    Return values:

    E_coh : float
        The cohesive energy of the system, which is
        E_tot / N_H2O since E_tot_H2O is zero with
        the SCME potential function.

    Usage example:

        >>> from testsuite.scme.ice_Ecoh_mmmm import ice_Ecoh_mmmm
        >>> E_coh = ice_Ecoh_mmmm()
        >>> print("E_coh = ", E_coh)
    '''
    parprint("\nMM ice cohesive energy calculation")
    parprint("----------------------------------\n")

    # Load periodic ice structure.
    # 96 molecules total.
    geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                       "../../structures/ice.xyz")
    atoms = read(geo)

    # Extend z axis to create a slab set PBC to 2D.
    atoms.cell[2] += [0, 0, 15]
    atoms.pbc = [True, True, False]
    atoms.center()

    # Calculate MM-MM interaction energy.
    mmcalc = SCME_PS(atoms=atoms,
                     qm_pbc=atoms.pbc,
                     qmmm=False,
                     NC=np.array(Nc),
                     dms=False,
                     irigidmolecules=True,
                     system=system,
                     para_set=para_set)
    atoms.calc = mmcalc

    # Calculate cohesive energy.
    E_tot = atoms.get_potential_energy()
    E_coh = E_tot / 96

    return E_coh
