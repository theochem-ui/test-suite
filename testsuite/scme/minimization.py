import os
import pandas as pd
import numpy as np
from ase_interface import SCME_PS
from ase.io import read
from ase.parallel import parprint
from ase.constraints import FixBondLengths
from ase.optimize import BFGS

def molpairs(atoms):
    rattle = ([((3 * i + j), (3 * i + (j + 1) % 3))
               for i in range((len(atoms)) // 3)
               for j in [0, 1, 2]])
    return rattle

def minimization(geo=None,
                 a=15.,
                 fmax=0.01,
                 system=np.ones(1) * 1000,
                 para_set='A'):
    '''SCME water structure energy minimization.

    Performs an energy minimization calculation on the
    "bag" structure of 6 water molecules (if no other
    water structure is given via `geo`).

    You can pass your own dimer geometry but are expected to
    prepare it such that the atomic sequence is
    [O_1 H_11 H_11 O_2 H_21 H_22].

    Parameters:

    geo : str
        File path to the geometry you want to use.
        Atomic sequence must be [O_1 H_11 H_11 O_2 H_21 H_22].
        Default / None: 6 molecule "bag" structure.

    a : float
        The *a* cell length parameter of the cubic box that
        the dimer will be placed in.

    fmax : float
        Limit to which forces should be optimized.

    system : int
        SCME fit parameter set. Default: PBE-SCME values.

    Return values:

    results : obj
        pandas.DataFrame object holding the final energy
        results. Columns:
            E_tot: total energy in eV.
            E_es: electrostatic interaction energy in eV.
            E_ds: dispersion interaction energy in eV.
            E_rp: repulsion interaction energy in eV.

    Usage example:

        >>> from testsuite.scme.minimization import minimization
        >>> df = minimization()
        >>> print(df)
    '''
    parprint("\nSCME structure energy minimization test")
    parprint("---------------------------------------\n")

    # Load model system.
    if not geo:
        geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                           "../../structures/bag.xyz")
    atoms = read(geo)
    atoms.pbc = False
    atoms.set_cell([a, a, a])
    atoms.center()

    # Set bond length constraints.
    tuples = molpairs(atoms)
    atoms.constraints = FixBondLengths(tuples)

    # Set up SCME calculator.
    mmcalc = SCME_PS(atoms=atoms,
                     qm_pbc=atoms.pbc,
                     qmmm=False,
                     NC=np.array([0, 0, 0]),
                     dms=False,
                     irigidmolecules=True,
                     system=system,
                     para_set=para_set)
    atoms.calc = mmcalc

    # Perform minimization.
    trajPath = os.path.join(os.getcwd(), "minimization.traj")
    dyn = BFGS(atoms, trajectory=trajPath)
    dyn.run(fmax=fmax)

    # Extract energy decomposition.
    E_tot = atoms.get_potential_energy()
    E_es = atoms.calc.energy_ES
    E_ds = atoms.calc.energy_DS
    E_rp = atoms.calc.energy_RP

    # Save results.
    columns = ["E_tot", "E_es", "E_ds", "E_rp"]
    results = pd.DataFrame([[E_tot, E_es, E_ds, E_rp]], columns=columns)

    return results
