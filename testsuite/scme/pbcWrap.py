import os
import pandas as pd
import numpy as np
from ase_interface import SCME_PS
from ase.io import read
from ase.parallel import parprint

def pbcWrap(a=10.,
            dmax=30.,
            dinc=0.1,
            numerical=False,
            debug_numerical=[1, 1, 1],
            force_select=0,
            system=np.ones(1) * 1000,
            para_set='A'):
    '''Test PBCs of the SCME implementation.

    Test wrapping around the box via periodic boundary
    conditions using SCME and a specific water dimer
    configuration.

    The energy output is generated for the entire system.
    For the forces, only the sum over the forces for
    1 molecule are given, and only the x/y/z component
    is selected for output by setting force_select=0/1/2.

    Parameters:

    a : float
        The *a* cell length parameter of the cubic box that
        the dimer will be placed in.

    dmax : float
        Maximum total distance that the dimer will be separated.

    dinc : float
        Increment with which distance will be increased.

    numerical : bool
        Calculate forces analytically (False) or numerically (True).

    debug_numerical : list[3]
        List used to zero certain energy terms to get the
        numerical forces from only the electrostatics (ES),
        dispersion (DS), or repulsion (RP). E.g. passing
        [1, 1, 1] would calculate the forces on the total energy
        as usual; [1, 0, 0] only from the ES; [0, 1, 0] only
        from the DS; and so on.

    force_select : int
        Set to 0, 1, or 2 to receive the x, y, or z component
        of the first molecule as the force output.
        Default: 0 (i.e. x component)

    system : int
        SCME fit parameter set. Default: PBE-SCME values.

    Return values:

    results : obj
        pandas.DataFrame object holding the final energy
        results. Columns:
            E_tot: total energy in eV.
            E_es: electrostatic interaction energy in eV.
            E_ds: dispersion interaction energy in eV.
            E_rp: repulsion interaction energy in eV.
            F_tot: the sum over the norms of the total forces.
            F_es: the sum over the norms of the electrostatic forces.
            F_ds: the sum over the norms of the dispersion forces.
            F_rp: the sum over the norms of the repulsion forces.

    Usage example:

        >>> from testsuite.scme.pbcWrap import pbcWrap
        >>> import matplotlib.pyplot as plt
        >>> df = pbcWrap(a=20., dmax=50)
        >>> plt.plot(df["z_coord"], df["E_rp"])
        >>> plt.show()
    '''
    parprint("\nSCME PBC wrapping test")
    parprint("----------------------\n")

    # Load model system.
    geo = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        "../../structures/dimer.xyz")
    atoms = read(geo)
    atoms.pbc = True
    atoms.set_cell([a, a, a])
    atoms.center()

    # Calculate number of distance adjustments.
    ninc = int(dmax / dinc)

    # Initialize results list.
    results = []

    # Calculate results for increasing separation distance.
    for i in range(0, ninc):

        # Adjust positions.
        pos = atoms.get_positions()
        pos[:3] += np.array([0, 0, dinc])
        atoms.set_positions(pos)

        # z coordinate of oxygen atom.
        z_coord = atoms[0].position[2]

        # Set up SCME calculator.
        mmcalc = SCME_PS(atoms=atoms,
                         qm_pbc=atoms.pbc,
                         qmmm=False,
                         NC=np.array([0, 0, 0]),
                         dms=False,
                         irigidmolecules=True,
                         numerical=numerical,
                         debug_numerical=debug_numerical,
                         system=system,
                         para_set=para_set)
        atoms.calc = mmcalc

        # Get energy values.
        E_tot = atoms.get_potential_energy()
        E_es = atoms.calc.energy_ES
        E_ds = atoms.calc.energy_DS
        E_rp = atoms.calc.energy_RP

        # Get force values.
        F_tot = sum(atoms.calc.forces_TOT.reshape(-1,3)[:3])[force_select]
        F_es = sum(atoms.calc.forces_ES.reshape(-1,3)[:3])[force_select]
        F_ds = sum(atoms.calc.forces_DS.reshape(-1,3)[:3])[force_select]
        F_rp = sum(atoms.calc.forces_RP.reshape(-1,3)[:3])[force_select]

        # Log results.
        results.append([z_coord, E_tot, E_es, E_ds, E_rp, F_tot, F_es, F_ds, F_rp])

    # Convert to pandas DataFrame.
    columns = ["z_coord", "E_tot", "E_es", "E_ds", "E_rp", "F_tot", "F_es", "F_ds", "F_rp"]
    df = pd.DataFrame(results, columns=columns)

    return df
